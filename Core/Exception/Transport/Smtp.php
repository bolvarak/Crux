<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Transport Namespace //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Crux\Core\Exception\Transport;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Transport\Smtp Class Definition //////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Smtp extends Exception
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} /// End Crux\Core\Exception\Transport\Smtp Class Definition ////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
