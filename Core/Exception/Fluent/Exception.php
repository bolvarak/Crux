<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Fluent Namespace /////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Crux\Core\Exception\Fluent;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Fluent\Exception Class Definition ////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Exception extends \Crux\Core\Exception
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} /// End Crux\Core\Exception\Fluent\Exception Class Definition //////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
