<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Crypto Namespace /////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Crux\Core\Exception\Crypto;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Crypto\MCrypt Class Definition ///////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class MCrypt extends Exception
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} /// End Crux\Core\Exception\Crypto\MCrypt Class Definition /////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
