<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Token Namespace //////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Crux\Core\Exception\Token;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Token\Exception Class Definition /////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Exception extends \Crux\Core\Exception
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} /// End Crux\Core\Exception\Token\Exception Class Definition ///////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
