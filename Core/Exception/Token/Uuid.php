<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Token Namespace //////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Crux\Core\Exception\Token;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Token\Uuid Class Definition //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Uuid extends Exception
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} /// End Crux\Core\Exception\Token\Uuid Class Definition ////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
