<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Provider\Sql\Engine Namespace ////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Crux\Core\Exception\Provider\Sql\Engine;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Exception\Provider\Sql\Engine\Lite Class Definition ////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Lite extends \Crux\Core\Exception\Provider\Sql\Engine
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} /// End Crux\Core\Exception\Provider\Sql\Engine\Lite Class Definition //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
