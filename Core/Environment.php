<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core Namespace //////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Crux\Core;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Imports //////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

use Crux\Core;
use Crux\Collection;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Crux\Core\Environment Class Definition ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Environment
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Magic Methods ////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This method provides a dynamic construct for calling environments
	 * @access public
	 * @name \Crux\Core\Environment::__callStatic()
	 * @package Crux\Core\Environment
	 * @param string $strMethod
	 * @param array $arrArguments
	 * @return string
	 * @static
	 * @uses \Crux\Core\Environment::exists()
	 * @uses strtolower()
	 * @uses substr()
	 * @uses substr_replace()
	 * @uses isset()
	 */
	public static function __callStatic(string $strMethod, array $arrArguments): string
	{
		// Check the method
		if (($strEnvironment = self::exists($strMethod, true)) !== false) { // Check for a direct environment call
			// We're done, the environment exists
			return $strEnvironment;
		} elseif (strtolower(substr($strMethod, 0, 3)) === 'get') {         // Check for a getter
			// Check for the environment
			if (($strEnvironment = self::exists(substr_replace($strMethod, '', 0, 3), true)) !== false) {
				// We're done, the environment exists
				return $strEnvironment;
			} else {
				// TODO - Throw Exception
			}
		} elseif (strtolower(substr($strMethod, 0, 3)) === 'set') {         // Check for a setter
			// Set the environment into the instance
			self::$mEnvironments[strtolower(substr_replace($strMethod, '', 0, 3))] = (isset($arrArguments[0]) ? $arrArguments[0] : null);
		} else {
			// TODO - Throw Exception
		}
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Properties ///////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This property contains the list of environments available to the instance
	 * @access protected
	 * @name \Crux\Core\Environment::$mEnvironments
	 * @package Crux\Core\Environment
	 * @static
	 * @var \Crux\Collection\Map
	 */
	protected static $mEnvironments;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Constructor //////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This method initializes the Environment container
	 * @access public
	 * @name \Crux\Core\Environment::Initialize()
	 * @return void
	 * @static
	 * @uses \Crux\Collection\Map::__construct()
	 * @uses \Crux\Collection\Map::set()
	 */
	public static function Initialize()
	{
		// Set the environments
		self::$mEnvironments = new Collection\Map();
		// Create our defaults
		self::$mEnvironments
			->set('devel', 'Development')
			->set('production', 'Production')
			->set('sandbox', 'Sandbox')
			->set('staging', 'Staging')
			->set('test', 'Test');
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Public Methods ///////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This method adds an environment to the instance if it doesn't already exist
	 * @access public
	 * @name \Crux\Core\Environment::add()
	 * @package Crux\Core\Environment
	 * @param string $strEnvironment
	 * @param string $strDescription
	 * @return void
	 * @static
	 * @uses \Crux\Core\Environment::exists()
	 * @uses strtolower()
	 * @uses \Crux\Collection\Map::set()
	 */
	public static function add(string $strEnvironment, string $strDescription)
	{
		// Check to see if the environment exists
		if (self::exists($strEnvironment) === false) {
			// Add the environment
			self::$mEnvironments->set(strtolower($strEnvironment), $strDescription);
		}
	}

	/**
	 * This method iterates over the environments and determines if an environment exists
	 * @access public
	 * @name \Crux\Core\Environment::exists()
	 * @package Crux\Core\Environment
	 * @param string $strEnvironment
	 * @param bool $blnReturnValue [false]
	 * @return bool|mixed
	 * @static
	 * @uses \Crux\Collection\Map::searchMapForKey()
	 */
	public static function exists(string $strEnvironment, bool $blnReturnValue = false)
	{
		// Check the name
		if (($strName = self::$mEnvironments->search($strEnvironment)) !== null) {
			// We're done
			return ($blnReturnValue ? self::$mEnvironments->get($strName) : true);
		}
		// We're done, the environment doesn't exist
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Getters //////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This method returns an environment from the instance if it exists
	 * @access public
	 * @name \Crux\Core\Environment::get()
	 * @package Crux\Core\Environment
	 * @param string $strEnvironment
	 * @return string
	 * @static
	 * @uses \Crux\Collection\Map::get()
	 */
	public static function get(string $strEnvironment): string
	{
		// Return the description
		return self::$mEnvironments->get($strEnvironment);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Setters //////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This method sets an environment into the instance, overriding any existing environments
	 * @access public
	 * @name \Crux\Core\Environment::set()
	 * @package Crux\Core\Environment
	 * @param string $strEnvironment
	 * @param string $strDescription
	 * @return void
	 * @static
	 * @uses strtolower()
	 * @uses \Crux\Collection\Map::set()
	 */
	public static function set(string $strEnvironment, string $strDescription)
	{
		// Set the environment into the instance
		self::$mEnvironments->set(strtolower($strEnvironment), $strDescription);
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} /// End Crux\Core\Environment Class Definition /////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
